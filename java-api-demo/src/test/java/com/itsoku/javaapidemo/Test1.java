package com.itsoku.javaapidemo;

import com.zaxxer.hikari.HikariDataSource;
import org.apache.shardingsphere.driver.api.ShardingSphereDataSourceFactory;
import org.apache.shardingsphere.infra.config.RuleConfiguration;
import org.apache.shardingsphere.infra.config.algorithm.ShardingSphereAlgorithmConfiguration;
import org.apache.shardingsphere.infra.config.props.ConfigurationPropertyKey;
import org.apache.shardingsphere.sharding.api.config.ShardingRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.rule.ShardingTableRuleConfiguration;
import org.apache.shardingsphere.sharding.api.config.strategy.sharding.StandardShardingStrategyConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * <b>author</b>：路人 <br>
 * <b>site</b>：<a href="http://itsoku.com/">Java充电社官网</a> <br>
 * <b>git</b>：<a href="https://gitee.com/javacode2018">git</a>
 */
public class Test1 {

    static DataSource dataSource;

    //@BeforeAll 标注的方法会在  @Test 标注的方法被执行前执行一次
    @BeforeAll
    public static void init() throws SQLException {
        /**
         * 1、创建datasource
         */
        Map<String, DataSource> dataSourceMap = createDataSources();

        /**
         * 2、构建具体规则
         */
        Collection<RuleConfiguration> ruleConfigs = new ArrayList<>();

        /**
         * 2.1、创建分片规则
         */
        ShardingRuleConfiguration shardingRuleConfiguration = new ShardingRuleConfiguration();

        //2.2、创建t_user表的分片规则
        ShardingTableRuleConfiguration userShardingTableRuleConfiguration =
                new ShardingTableRuleConfiguration("t_user", //逻辑表
                        "ds1.t_user_$->{0..1}"); //实际数据节点
        //2.3、配置t_user的分表规则，分片字段是：user_id,分片算法是：userShardingAlgorithm（这个算法名称是自定义的，后面会定义这个名称对应的具体算法）
        userShardingTableRuleConfiguration.setTableShardingStrategy(
                new StandardShardingStrategyConfiguration(
                        "id",
                        "userShardingAlgorithm"));
        //2.3、将t_user表的分片规则加入到shardingRuleConfiguration中
        shardingRuleConfiguration.getTables().add(userShardingTableRuleConfiguration);

        //2.4、定义用户表具体的算法
        Properties userShardingAlgorithmProperties = new Properties();
        userShardingAlgorithmProperties.put(
                "algorithm-expression",
                "t_user_$->{id%2}"
        );
        ShardingSphereAlgorithmConfiguration userShardingAlgorithm =
                new ShardingSphereAlgorithmConfiguration("INLINE",
                        userShardingAlgorithmProperties);

        //2.5、将定义好的 userShardingAlgorithm 算法加入到算法列表中（算法名称->算法）
        shardingRuleConfiguration.getShardingAlgorithms().
                put("userShardingAlgorithm", userShardingAlgorithm);

        //2.6、将分片规则加入规则列表
        ruleConfigs.add(shardingRuleConfiguration);

        /**
         * 3、建属性配置
         */
        Properties props = new Properties();
        props.put(ConfigurationPropertyKey.SQL_SHOW.getKey(), "true");

        /**
         * 4、创建数据源
         */
        dataSource = ShardingSphereDataSourceFactory.createDataSource(
                "shardingsphere-demo-db",
                null,
                dataSourceMap,
                ruleConfigs,
                props);
    }


    private static Map<String, DataSource> createDataSources() {
        Map<String, DataSource> dataSourceMap = new HashMap<>();
        // 配置第 1 个数据源
        HikariDataSource dataSource1 = new HikariDataSource();
        dataSource1.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource1.setJdbcUrl("jdbc:mysql://localhost:3306/ds_shardingsphere5?characterEncoding=UTF-8");
        dataSource1.setUsername("root");
        dataSource1.setPassword("root123");
        dataSourceMap.put("ds1", dataSource1);
        return dataSourceMap;
    }

    @Test
    public void m1() throws Exception {
        String sql = "insert into t_user values (?,?),(?,?)";
        try (Connection connection = dataSource.getConnection();) {
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement(sql);

            int parameterIndex = 1;
            ps.setInt(parameterIndex++, 1);
            ps.setString(parameterIndex++, "user-1");
            ps.setInt(parameterIndex++, 2);
            ps.setString(parameterIndex++, "user-2");

            ps.executeUpdate();
            connection.commit();
        }
    }

    @Test
    public void m2() throws Exception {
        String sql = "select id,name from t_user where id = 1";
        try (Connection connection = dataSource.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                long id = rs.getLong("id");
                String name = rs.getString("name");
                System.out.println(String.format("id:%s, name:%s", id, name));
            }
        }
    }

}
